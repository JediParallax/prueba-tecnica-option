import Vuex from 'vuex'
import Vue from 'vue'
import axios from 'axios'

Vue.use(Vuex)

const store =  new Vuex.Store({
    state: {
        results: null,
        err: null
    },
    actions: {
        async getResults({commit}, q){
            try{
                const response = await axios.get(`http://localhost:3000/search`, {
                    params: {
                        q: q
                    }
                })
                if(response.data.length>0){
                    commit('SET_RESULTS', response.data)
                    commit('SET_RESULTS_ERRORS', null)
                }else{
                    commit('SET_RESULTS', null)
                    commit('SET_RESULTS_ERRORS', `No se encontraron resultados para el periodo seleccionado`)
                }
            }catch(e){
                if(e.message.includes(404)){
                    commit('SET_RESULTS_ERRORS', `No se encontraron resultados para el periodo seleccionado`)
                }else{
                    commit('SET_RESULTS_ERRORS', e.message)
                }
                commit('SET_RESULTS', null)
            }
        }
    },
    mutations:{
        SET_RESULTS(state, payload){
            state.results = payload
        },
        SET_RESULTS_ERRORS(state, e){
            state.err = e
        }
    }
})

export default store