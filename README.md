# youtube-bar
El front tambien está subido al siguiente sitio web:
https://youthful-darwin-20beb6.netlify.app/

(Node debe estar corriendo antes para que funcione)
## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
